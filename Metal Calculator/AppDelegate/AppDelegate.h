//
//  AppDelegate.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#define NewsAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]

@interface AppDelegate : UIResponder <UIApplicationDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UIView   *footerView;

@property (strong, nonatomic) IBOutlet UINavigationController *navController;


@property (nonatomic, weak)   IBOutlet      UIButton        *btnMail;
@property (nonatomic, weak)   IBOutlet      UIButton        *btnCall;
@property (nonatomic, weak)   IBOutlet      UIButton        *btnWebsite;
@property (nonatomic, weak)   IBOutlet      UIButton        *btnAbout;

@property (nonatomic, strong)          MFMailComposeViewController  *mc;
-(void)showFooterView;
-(void)tabBarSetUp:(NSString *)tabAction;
@end

