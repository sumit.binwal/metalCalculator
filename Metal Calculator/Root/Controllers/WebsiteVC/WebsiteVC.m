//
//  WebsiteVC.m
//  animation3d
//
//  Created by Tanvi on 26/11/14.
//  Copyright (c) 2014 Konstant Info Solutions. All rights reserved.
//

#import "WebsiteVC.h"

@interface WebsiteVC ()
{
    IBOutlet UIWebView *webVAboutUs;
}
@property (nonatomic,retain)IBOutlet UILabel *lblNoInternetConnection;

@end

@implementation WebsiteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [webVAboutUs loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.essexweldsolutions.com"]]];
    [self.navigationItem setTitle:@"WebSite"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClick)];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    [CommonFunctions showActivityIndicatorWithText:@""];

}
-(void)backButtonClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [CommonFunctions removeActivityIndicator];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonFunctions removeActivityIndicator];
    NSLog(@"%@",error);
}


@end
