//
//  CalculationVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CalculationVC.h"
#import "CalcResultVC.h"
#import "RequestVC.h"
@interface CalculationVC ()
{
    UITextField *currentTextField;
    UIBarButtonItem *cancelBtn;
    UIBarButtonItem *doneBtn;
    float value1;
    NSString *calculateValue;
    IBOutlet UILabel *resultLbl;
    IBOutlet UIButton *calcuclateButton;
    IBOutlet UILabel *lblRequiredInch;
    IBOutlet UILabel *lblLength;
        IBOutlet UILabel *lblWidth;
    BOOL flatbatFlag;
    IBOutlet UITextField *txtWdth;
    IBOutlet UILabel *txtWinch;
    

    IBOutlet UILabel *lblNote;
    IBOutlet NSLayoutConstraint *requredConstrentHeight;
    float calculateResult;
}
@end

@implementation CalculationVC
@synthesize segmentController;
@synthesize lblUnit1;
@synthesize lblUnit2;
@synthesize lblUnit3;
@synthesize scrollVw;
@synthesize strDensity;
@synthesize strMaterial;
@synthesize lblMaterial;
@synthesize lblTypeDensity;
@synthesize imgVw;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    
    NSLog(@"DataDict :%@",dataDictMA);
    
    [imgVw setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"imgName"]]]];
    
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"shape"]isEqualToString:@"flatbed Laser"])
    {
        [lblTypeDensity setText:@"Thickness :"];
        _lblDensity.text=strDensity;
        [lblNote setHidden:YES];
        [_txtLength setHidden:YES];
        

        [lblLength setHidden:YES];
    
        requredConstrentHeight.constant=50;

        [lblUnit2 setHidden:YES];
        [lblUnit3 setHidden:YES];

        _txtLength.text=@"44";
        txtWdth.text=@"44";
        lblMaterial.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"Material"];
    }
    else
    {
        if ([[dataDictMA objectForKey:@"shapeName"] isEqualToString:@"Square/Rectangle Tube "]) {
            if([[[NSUserDefaults standardUserDefaults]valueForKey:@"imgName"]isEqualToString:@"squaretube"])
            {
                [lblWidth setHidden:NO];
                [txtWdth setHidden:NO];
                [txtWinch setHidden:NO];
            }
            if([[dataDictMA objectForKey:@"shape"] isEqualToString:@"square"])
            {
                [lblWidth setHidden:YES];
                [txtWdth setHidden:YES];
                [txtWinch setHidden:YES];
                [lblLength setText:@"Width"];
                txtWdth.text=@"11";
            }
            else if([[dataDictMA objectForKey:@"shape"] isEqualToString:@"rectangle"])
            {
                [lblWidth setHidden:NO];
                [txtWdth setHidden:NO];
                [txtWinch setHidden:NO];
            }

        }
        else
        {
            txtWdth.text=@"11";

        }
    
        
        [lblTypeDensity setText:@"Thickness :"];
    _lblDensity.text=strDensity;
    lblMaterial.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"Material"];
    }
    [scrollVw setScrollEnabled:NO];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if(screenBounds.size.height < 568)
    {
        NSLog(@"this is iphone 4");
    [self.scrollVw setScrollEnabled:YES];
    
    [self.scrollVw setContentSize:CGSizeMake(320.0f, 500.0f)];
    }
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Calculate" ];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    [numberToolbar setBarTintColor:[UIColor colorWithRed:113.0f/255.0f green:138.0f/255.0f blue:173.0f/255.0f alpha:1.0f]];
    [numberToolbar setTranslucent:NO];
    
    cancelBtn = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)];
    cancelBtn.tintColor = [UIColor whiteColor];
    doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    doneBtn.tintColor = [UIColor whiteColor];
    
    numberToolbar.items = [NSArray arrayWithObjects:cancelBtn, [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneBtn, nil];
    
    [numberToolbar sizeToFit];
    
    _RequiredInch.inputAccessoryView = numberToolbar;
    
    _txtLength.inputAccessoryView = numberToolbar;
    txtWdth.inputAccessoryView=numberToolbar;
    calcuclateButton.layer.cornerRadius=8;

}

-(void)cancelNumberPad
{
    [currentTextField resignFirstResponder];
    
    [self scrollToNormalView];
    [scrollVw setScrollEnabled:NO];
}

-(void)doneWithNumberPad{
    
    if (currentTextField == _RequiredInch) {
        if(flatbatFlag)
        {
            [currentTextField resignFirstResponder];
            [self scrollToNormalView];
            [scrollVw setScrollEnabled:NO];
        }
        else
        {
        [_txtLength becomeFirstResponder];
        }
    }
    else if (currentTextField==_txtLength)
    {
        [txtWdth becomeFirstResponder];
    }
    else if (currentTextField == txtWdth)
    {
        [currentTextField resignFirstResponder];
        [self scrollToNormalView];
        [scrollVw setScrollEnabled:NO];
        
    }
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segController:(UISegmentedControl *)sender forEvent:(UIEvent *)event {
    if(segmentController.selectedSegmentIndex==0)
    {
        lblUnit1.text=@"INCH";
        lblUnit2.text=@"INCH";
        txtWinch.text=@"INCH";
        
    }
    else if (segmentController.selectedSegmentIndex==1)
    {

        lblUnit1.text=@"MM";
        lblUnit2.text=@"MM";
        txtWinch.text=@"MM";
    }
    
}
#pragma mark - UIScroll View Deligate
- (void) scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (self.scrollVw.contentSize.height == 800.0f)
        difference = 250.0f;
    else
        difference = 200.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 190.0f;
    CGFloat y = viewCenterY - avaliableHeight / 2.0f;
    
    if (y < 0)
        y = 0;

    [self.scrollVw setContentOffset:CGPointMake(0, y) animated:TRUE];
}


-(void)scrollToNormalView
{
    [self.scrollVw setContentSize:CGSizeMake(320.0f, 600.0f)];
    [self.scrollVw setContentOffset:CGPointZero];
}

#pragma mark - UI TextField Deligate Method
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentTextField = textField;
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"shape"]isEqualToString:@"flatbed Laser"])
    {
        if (textField == _RequiredInch)
        {
            [doneBtn setTitle:@"Done"];
            flatbatFlag=YES;
        }
    }
    else
    {
    if (textField == txtWdth)
        [doneBtn setTitle:@"Done"];
    else
        [doneBtn setTitle:@"Next"];
    }
    [self scrollViewToCenterOfScreen:textField];
    [scrollVw setScrollEnabled:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
    [self.scrollVw setScrollEnabled:YES];
    [self.scrollVw setContentSize:CGSizeMake(280, 700.0f)];
    NSLog(@"Scrool View Width : %f",scrollVw.frame.size.width);
    NSLog(@"View Width : %f",self.view.frame.size.width);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _VwCntrller.frame = CGRectMake(0, 0, scrollVw.frame.size.width, scrollVw.frame.size.height);
    [scrollVw addSubview:_VwCntrller];
}

-(BOOL)checkValueinTextField
{
    bool sucess=false;
    if(!_RequiredInch.text.length>0)
    {
        currentTextField=_RequiredInch;
        [CommonFunctions alertTitle:@"Enter Required Inches to Continue" withMessage:nil withDelegate:self];
        return  sucess;
    }
    if(!_txtLength.text.length>0)
    {
                currentTextField=_txtLength;
                [CommonFunctions alertTitle:@"Enter Length to Continue" withMessage:nil withDelegate:self];
        return  sucess;
    }
    if(!txtWdth.text.length>0)
    {
        currentTextField=txtWdth;
        [CommonFunctions alertTitle:@"Enter Width to Continue" withMessage:nil withDelegate:self];
        return  sucess;
    }
        return sucess=YES;
}
- (IBAction)calculateButtonClicked:(id)sender {
    
    [self.view endEditing:YES];
       if([self checkValueinTextField])
    {
        if(segmentController.selectedSegmentIndex==0)
        {
            calculateResult=[_RequiredInch.text floatValue]*[materialCost floatValue];
            resultLbl.text=[NSString stringWithFormat:@"$ %.02f Cost/%@ Inch",calculateResult,_RequiredInch.text];
            [CommonFunctions alertTitle:@"Result" withMessage:[NSString stringWithFormat:@"$ %.02f Cost/%@ Inch",calculateResult,_RequiredInch.text]];
        }
        else
        {
           value1=[_RequiredInch.text floatValue]*0.039370;
            calculateResult=value1*[materialCost floatValue];
            resultLbl.text=[NSString stringWithFormat:@" %.02f Cost/%.04f Inch",calculateResult,value1];
            [CommonFunctions alertTitle:@"Result" withMessage:[NSString stringWithFormat:@"$ %.02f Cost/%.04f Inch",calculateResult,value1]];
        }
        [resultLbl setHidden:NO];
    }
    if([UIScreen mainScreen].bounds.size.height<568)
    {
    [scrollVw setScrollEnabled:YES];
    }
    else
    {
    [self scrollToNormalView];
    [scrollVw setScrollEnabled:NO];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(currentTextField==_RequiredInch)
    {
        [_RequiredInch becomeFirstResponder];
    }
    if(currentTextField==_txtLength)
    {
        [_txtLength becomeFirstResponder];
    }
  else if (currentTextField==txtWdth)
  {
      [txtWdth becomeFirstResponder];
  }
    
}
-(IBAction)requestButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    [scrollVw setContentOffset:CGPointZero];
    if(resultLbl.text.length>8)
    {
    RequestVC *request=[[RequestVC alloc]init];
        if(segmentController.selectedSegmentIndex==0)
        {
            request.resultValue=[NSString stringWithFormat:@"$ %.02f/%@ Inch",calculateResult,_RequiredInch.text];
            NSLog(@"%@",[NSString stringWithFormat:@"$ %.02f/%@ Inch",calculateResult,_RequiredInch.text]);
            [dataDictMA setObject:_RequiredInch.text forKey:@"rInch"];
        }
        else
        {
            request.resultValue=[NSString stringWithFormat:@"$ %.02f/%.02f Inch ",calculateResult,value1];
            NSLog(@"%@",[NSString stringWithFormat:@"$ %.02f/%.02f Inch ",calculateResult,value1]);
            [dataDictMA setObject:[NSString stringWithFormat:@"$ %.02f",value1] forKey:@"rInch"];
        }

    [self.navigationController pushViewController:request animated:YES];
    }
   else
   {
       [CommonFunctions alertTitle:@"Please Calculate Value First" withMessage:nil];
   }
}
@end
