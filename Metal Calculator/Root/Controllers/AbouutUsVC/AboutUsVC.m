//
//  AboutUsVC.m
//  animation3d
//
//  Created by Tanvi on 26/11/14.
//  Copyright (c) 2014 Konstant Info Solutions. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()
{
    IBOutlet UIWebView *webVAboutUs;
}
@property (nonatomic,retain)IBOutlet UILabel *lblNoInternetConnection;
@end

@implementation AboutUsVC
#pragma mark -  Application lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
    [webVAboutUs loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://essexweldsolutions.com/about-us"]]];
    [self.navigationItem setTitle:@"About US"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    [CommonFunctions showActivityIndicatorWithText:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -  View sestup methods

-(void)setUpView{
    self.lblNoInternetConnection.hidden=YES;
 
            self.lblNoInternetConnection.hidden=YES;
            
            [webVAboutUs loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.essexweldsolutions.com/about-us"]]];

    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [CommonFunctions removeActivityIndicator];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonFunctions removeActivityIndicator];
    NSLog(@"%@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
