
//
//  SelectMaterialVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectMaterialVC.h"
#import "MaterialCell.h"
#import "SelectTypeVC.h"

#import "CalculationVC.h"
@interface SelectMaterialVC ()
{
    NSMutableArray *dataMA;
}
@end

@implementation SelectMaterialVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
       UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    [self.navigationItem setTitle:@"Select Material" ];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        dataMA=[[NSMutableArray alloc]init];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"shape"]isEqualToString:@"Tube Laser"])
    {
        
        [dataMA addObject:@"Stainless Steel"];
        [dataMA addObject:@"Copper"];
        [dataMA addObject:@"Brass"];
        [dataMA addObject:@"Aluminum"];
        [dataMA addObject:@"Steel"];
    }
    else
    {

        [dataMA addObject:@"Stainless Steel"];
        [dataMA addObject:@"Aluminum"];
        [dataMA addObject:@"Hot Rolled Steel"];
        [dataMA addObject:@"Galvanized Steel"];
        [dataMA addObject:@"Steel"];
    }

    

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Deligate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataMA count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MaterialCell";
    MaterialCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MaterialCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];

    }
   [cell.labelMaterial setText:[dataMA objectAtIndex:indexPath.row]];
   
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dataMA objectAtIndex:indexPath.row],@"Material", nil];
    
    [dataDictMA setValuesForKeysWithDictionary:tempDict];
    if([[dataMA objectAtIndex:indexPath.row] isEqualToString:@"Steel"])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[dataMA objectAtIndex:indexPath.row] forKey:@"Material"];
    }

    SelectTypeVC *typeVC=[[SelectTypeVC alloc]init];
    [[NSUserDefaults standardUserDefaults]setValue:[dataMA objectAtIndex:indexPath.row] forKey:@"Material"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    [self.navigationController pushViewController:typeVC animated:YES];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
