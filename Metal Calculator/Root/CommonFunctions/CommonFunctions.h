//
//  CommonFunctions.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "AppDelegate.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CommonFunctions : NSObject
+(void) setNavigationBar:(UINavigationController*)navController;
+(void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg;
+(BOOL)isValueNotEmpty:(NSString*)aString;
+(BOOL)IsValidEmail:(NSString *)checkString;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate;
+ (void)showActivityIndicatorWithText:(NSString *)text;
+ (void)removeActivityIndicator;
@end
