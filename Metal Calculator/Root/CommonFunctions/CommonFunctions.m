//
//  CommonFunctions.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CommonFunctions.h"
#import "MBProgressHUD.h"
@implementation CommonFunctions
#define APPDELEGATE    ((AppDelegate *)[[UIApplication sharedApplication] delegate])
+(void) setNavigationBar:(UINavigationController*)navController
{
    
 
    navController.navigationBar.barTintColor = [UIColor blackColor];
    navController.navigationBar.tintColor=[UIColor whiteColor];
    [navController setNavigationBarHidden:FALSE];
    [navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_MEDIUM size:15],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    [navController.navigationBar setTranslucent:NO];

    
}
#pragma mark ReachabiltyCheck methods
+ (BOOL)isValueNotEmpty:(NSString*)aString{
    if (aString == nil || [aString length] == 0){
        
        return NO;
    }
    return YES;
}


+(BOOL)IsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:aTitle
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show];
}


+ (void)removeActivityIndicator {
    [MBProgressHUD hideAllHUDsForView:APPDELEGATE.window animated:YES];
    [MBProgressHUD hideHUDForView:APPDELEGATE.window animated:YES];
}

+ (void)showActivityIndicatorWithText:(NSString *)text {
    [self removeActivityIndicator];
    
    MBProgressHUD *hud   = [MBProgressHUD showHUDAddedTo:APPDELEGATE.window animated:YES];
    hud.labelText        = text;
    hud.detailsLabelText = NSLocalizedString(@"Please Wait...", @"");
}
/*!
 @function	removeActivityIndicator
 @abstract	removes the MBProgressHUD (if any) from window.
 */


+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg{
    [self alertTitle:aTitle withMessage:aMsg withDelegate:nil];
}


@end
